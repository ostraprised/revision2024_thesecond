# Revision2024 shader project v2

This is an attempt to learn Vulkan shader programming and Rust, before/during/after [Revision2024](https://2024.revision-party.net/).

## usage

``` shell
make shaders
cargo run
```

## Resources

https://vkguide.dev/
https://doc.rust-lang.org/book/ch01-00-getting-started.html
https://kylemayes.github.io/vulkanalia/introduction.html

## Concepts

```
---
App
---
│   -----
├── Scene - Wrapper for a pipeline + everything it needs
│   -----
│   │
│   │   --------
│   ├── Pipeline - Contains all of the configuration options needed to draw geometry
│   │   --------
│   │   │
│   │   │ -----------
│   │   └ ShaderStage - Assign shader module to specific Vertex/frag/... stage in pipeline
│   │     -----------
│   │       │
│   │       │   -------------
│   │       └── Shader Module - Contains shader byte code (from GLSL) for single shader 
│   │           -------------   module.
│   │                         - Can have multiple functions, each of which can be used 
│   │                           in a ShaderStage.
│   │
│   │
│   │   ---------------
│   ├── Pipeline Layout - Represents a sequence of descriptor sets with each having 
│   │   ---------------   a specific layout. This sequence of layouts is used to 
│   │       │             determine the interface between shader stages and 
│   │       │             shader resources. Each pipeline is created using a pipeline
│   │       │             layout.
│   │       │
│   │       │  ----------
│   │       └─ Descriptor - Handle or pointer to a resource (buffer, img, ...)
│   │          ----------
│   │
│   │
│   │  -------------
│   └─ Command Pools - Manage the memory that is used to store the buffers and 
│      -------------   command buffers are allocated from them.
│       │
│       │  ---------------
│       ├  Command Buffers - Holds drawing operations and memory transfers
│       │  ---------------
│       │
│       │  -------------------------
│       ├  Secondary Command Buffers
│       │  -------------------------
│
│
│
│
│  ---------
└─ SwapChain - Owns buffers we render to before visualizing on screen
   ---------
    │
    │  -----------
    └─ Framebuffer - 
       -----------

Uniform Buffer
    └── Uniforms - Globals similar to dynamic state variables that can be 
                   changed at drawing time to alter the behavior of your 
                   shaders without having to recreate them.

viewport

```

## Dependencies

- Remember to install vulkan validation layers

## Credits

### Images

- [Juniper img](https://pixabay.com/photos/juniper-common-juniper-evergreen-512877/)

### License
